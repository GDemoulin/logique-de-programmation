Début Recherche
	Début Variable	
		Text 				<- "" 		String
		Choice 				<- "" 		String
		Flag_FPosition		<- False	Booleen
		Flag_LPosition		<- False	Booleen
		Flag_Occurence		<- False	Booleen
		
		First_Position	<- -1 Numerique
		Last_Position	<- -1 Numerique
		Nombre_d'ocurrence	<- 0 Numerique
		Target				<- '' Caractère
 	Fin Variable
	
	Début Get_First (Text String, Target Caractère) Retourne Numerique
		Début Variable
			Position <- 0 Numerique
			Found <- -1 Numerique
		Fin variable
		
		Tant que Position != get_Longueur(Text) ET Found != 0
			Si Text[Position] = Target
				Alors
					Found <- Position +1
			Fin Si
			Position++
		Fin Tant Que
		
		Retourne Found
	Fin Get_First
	
	
	Début Get_Last (Text String, Target Caractère) Retourne Numerique
		Début Variable
			Position <- 0 Numerique
			Found <- -1 Numerique
		Fin variable
		
		Position<-get_Longueur(Text)
		Tant que Position != 0 ET Found != 0
			Si Text[Position] = Target
				Alors
					Found <- Position +1
			Fin Si
			Position--
		Fin Tant Que
		
		Retourne Found
	Fin Get_Last
	
	
	
	Début Get_Number (Text String, Target Caractère)
		Début Variable
			Nombre 		<- 0 Numerique
			Position 	<- 0 Numerique
		Fin Variable
		
		Pour Position Allant de get_Longueur(Text) -1  a 0 par pas de -1
			Si Text[Position] = Target
				Alors
					Nombre ++
			Fin Si
		Fin Pour
		Retourne Nombre
	Fin Get_Number
	
	
	Afficher "Veuillez entrer le Text: "
	Lire Text
	
	Tant que get_Longueur(Text) <= 0
		Afficher "Veuillez entrer un Text non vide"
		Lire Text
	Fin Tant que

	Afficher "Veiullez entrer votre cible: "
	Lire Target
	
	Tant que Target = ""
		Afficher "Veuillez entrer une cible valide"
		Lire Target
	Fin Tant que
	
	Afficher "Que voulez-vous faire?"
	Afficher "1. Afficher la Première position"
	Afficher "2. Afficher la Dernière position"
	Afficher "3. Afficher le nombre d'occurence"
	Afficher "Votre Choice: "
	Lire Choice
	
	Pour Position Allant de 0 à get_Longueur(Choice)
		CAS OU Choice[position]
			Cas "1"
				Flag_FPosition <- True
			Cas "2"
				Flag_LPosition <- True
			Cas "3"
				Flag_Occurence <- True
		Fin CAS OU
			
	Fin Pour
	
	
	Si Flag_FPosition
		First_Position <- Get_First (Text,target)
		Si First_Position != -1
			Alors
				Afficher "La première position de " & Target " est " & First_Position
			Sinon
				Afficher "Caractère non trouvé"
	Fin
	
	Si Flag_LPosition
		Last_Position <- Get_Last (Text,target)
		Si Last_Position != -1
			Alors
				Afficher "La derniere position de " & Target " est " & Last_Position
			Sinon
				Afficher "Caractère non trouvé"
	Fin
	
	Si Flag_Occurence
		Nombre_d'occurence <- Get_Number (Text,target)
		Afficher Target " apparait " & Nombre_d'ocurrence & " fois"
	Fin

Fin Recherche